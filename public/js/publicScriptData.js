//Al hacer click en el botón del formulario para reservar obtiene los valores de campos ocultos 
$('#submit').click(function(e){
	e.preventDefault();
	var people = $('#people').val();
	var available = $('#available').val();
	var total = parseInt(available) - parseInt(people);
	var form = $('form')[0];
    var formData = new FormData(form);
	var route ='http://localhost:8000/api/bookings';
	//si el número el número de asistentes es mayor a los disponibles, manda un error
	if (total<0) {$(".print-error-msg").find("ul").append('<li> No hay lugares suficientes</li>');}
	else{

	$.ajax({
		url: route,
		type: 'POST',
		dataType: 'json',
		contentType: false,
		processData: false,
		data: formData,
		success:function(data){
			//Si no hay errores al procesar la petición, se muestra un mensaje y se actualizan los lugares disponibles.
			if($.isEmptyObject(data.error)){
				$('#notify').append("<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i> Felicidades</h4>Su reservación se hizo correctamente</div>")
				$('.available').text('Lugares disponibles '+total);
	        }else{
				printErrorMsg(data.error);
	        }

		}
	});
	//función que retorna los errores de llenado de formulario.
	function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}
	}
});