// Obtiene todos los eventos
$(document).ready(function(){
	var dataTable = $('#data');
	var route = 'http://localhost:8000/api/events';
	var labelColor='label-success';
	var bookingsNumber=0;
	$.get(route, function(response){
		if(response.length>0){
			$(response).each(function(key, value){
				bookingsNumber=$(value.bookings).length;
				dataTable.append("<tr><td>"+value.name+"</td><td>"+value.start_date+" hasta "+value.end_date+"</td><td><span class='label "+labelColor+"'>"+value.status+"</span></td><td>"+bookingsNumber+"</td><td><button value="+value.id+" type='button' class='btn btn-primary editButton'><i class='fa fa-edit'></i></button><button value="+value.id+" type='button' class='btn btn-primary deleteButton'><i class='fa fa-trash'></i></button><button value="+value.id+" type='button' class='btn btn-primary bookingButton'><i class='fa fa-eye'></i></button></td></tr>")
			});			
		}
		else{
			dataTable.append("<tr id='first'><td></td><td>No hay eventos registrados</td><td></td><td></td></tr>")
		}
	});
});
// al hacer click en el botón de  editar, muestra un formulario con la información del evento
$("#data").on('click','button.editButton', function() {
	$('#myModal').modal('show');
	$('#type').val('edit');
	var id= $(this).val();
	$('#buttonId').val(id);
	var route = 'http://localhost:8000/api/events/'+id;
	$.get(route, function(response){
		$('#name').val(response.name)
		$('#descrip').val(response.descrip)
		$('#start_date').val(response.start_date)
		$('#end_date').val(response.end_date)
		$('#capacity').val(response.capacity)
		$('#principal').val(response.principal)
		$('#slider').val(response.slider)
		$('#principal').val(response.principal)
	});
})
// Al hacer click en el botón de eliminar, muestra un modal de confirmación
$("#data").on('click','button.deleteButton', function() {
	var id= $(this).val();
	$('#modalDelete').modal('show');
})

// al hacer click en el botón de ver reservaciones, muestra un modal con la información de las personas que reservan
$("#data").on('click','button.bookingButton', function() {
	var id= $(this).val();
	var route = 'http://localhost:8000/api/bookings/'+id;
	$.get(route, function(response){
		$('#modalBookings').modal('show');
		if(response.length>0){
			$(response).each(function(key, value){
				$('#bookingData').append("<tr><td>"+value.first_name+value.last_name+"</td><td>"+value.email+"</td><td>"+value.people+"</td></tr>")
			});			
		}
		else{
			$('#bookingData').append("<tr id='first'><td></td><td>No hay eventos registrados</td><td></td><td></td></tr>")
		}

	});
})
//Al confirmar la eliminación, se hace la petición delete al web service
$('#deleteSubmit').click(function(e){
	e.preventDefault();
	var id = $('#deleteId').val();
	var method ='delete';
	var route ='http://localhost:8000/api/events/'+id;
	$.ajax({
		url: route,
		type: method,
		dataType: 'json',
		contentType: false,
		processData: false,
		data: id,
		success:function(data){
			$('#modalDelete').modal('hide');
				$('#modalDelete').on('hidden.bs.modal', function (e) {
				  $(this)
				    .find("input")
				       .val('')
				       .end()
				})
			$('button[value='+id+']').parent().parent().remove()
		}
	});
});

//al hacer click en el botón guardar del modal, manda la petición post o patch dependiendo el caso.
$('#submit').click(function(e){
	e.preventDefault();
	var form = $('form')[1];
	var type = $('#type').val();
    var formData = new FormData(form);
	var name = $('#name').val();
	var start_date = $('#start_date').val();
	var end_date = $('#end_date').val();
	var capacity = $('#capacity').val();
	var principal = $('#principal').val();
	var method ='post';
	var route ='http://localhost:8000/api/events';
	if (type=='edit'){
		var id = $('#buttonId').val();
		route ='http://localhost:8000/api/events/'+id;
		method = 'patch'
	}
	var first = $.trim($('#data').text());
	$.ajax({
		url: route,
		type: method,
		dataType: 'json',
		contentType: false,
		processData: false,
		data: formData,
		success:function(data){
			if($.isEmptyObject(data.error)){
				if(first=='No hay eventos registrados')
					$('#first').remove();
				$('#myModal').modal('hide');
				$('#myModal').on('hidden.bs.modal', function (e) {
				  $(this)
				    .find("input,textarea,select")
				       .val('')
				       .end()
				    .find("input[type=checkbox], input[type=radio]")
				       .prop("checked", "")
				       .end();
				})
				if(data.id)
					$('#data').append("<tr><td>"+name+"</td><td>"+start_date+" hasta "+end_date+"</td><td><span class='label label-success'> Disponible</span></td><td>0</td><td><button value="+data.id+" type='button' class='btn btn-primary editButton'><i class='fa fa-edit'></i></button><button value="+data.id+" type='button' class='btn btn-primary deleteButton'><i class='fa fa-trash'></i></button><button value="+value.id+" type='button' class='btn btn-primary bookingButton'><i class='fa fa-eye'></i></button></td></tr>")
			}else{
				printErrorMsg(data.error);
	        }
		}
	});

});

function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}