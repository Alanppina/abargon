<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id')->comment('Identificador del registro');
            $table->integer('event_id')->unsigned()->comment('Llave foránea, Identificador de la tabla events');
            $table->string('first_name')->comment('Nombre o nombres de la persona que reserva');
            $table->string('last_name')->commnet('Apellido o apellidos de la persona que reserva');
            $table->string('email')->comment('Email de la persona que reserva');
            $table->smallInteger('people')->comment('Número de personas para la reservación');
            $table->timestamps();
        });

        Schema::table('bookings', function ($table){
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');;
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
