<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('Identificador del registro');
            $table->string('name')->comment('Nombre del usuario');
            $table->string('email')->unique()->comment('Email del usuario');
            $table->string('password')->comment('Contraseña de acceso');
            $table->rememberToken()->comment('Token de sesión');
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$2KMWaEa33GPIqQ3OcEd7UeGTubiWb/HHRnyW0/lsy5mQvSmDOrbJm',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
