<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id')->comment('identificador del registro');
            $table->integer('user_id')->unsigned()->comment('Llave foránea, identificador de la tabla users');
            $table->string('name')->comment('Nombre del evento');
            $table->string('descrip')->comment('Descripción del evento');
            $table->smallInteger('capacity')->comment('Capacidad total del evento (Max 8)');
            $table->date('start_date')->comment('Fecha de inicio del evento');
            $table->date('end_date')->comment('Fecha final del evento');
            $table->string('status')->comment('Estatus del evento: Disponible, Lleno');
            $table->string('thumbnail')->comment('URL de la imagen en miniatura');
            $table->string('slider')->comment('URL de la imagen del slider');
            $table->smallInteger('principal')->comment('Opción para poner la imagen en el slider principal: 0=no 1=si;');

            $table->timestamps();
        });
        Schema::table('events', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
