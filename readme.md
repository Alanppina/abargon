# **Á**bargon - Prueba técnica

Desarrollar una aplicación web para reservar eventos, en la cual usuarios anónimos podrán consultar fechas disponibles para eventos.

Realizada por - Alan Sinue Pastor Piña

![Screenshot](https://lh5.googleusercontent.com/Zqz5IJD_hwBEDlwMUXtADEFA49zEwFOc2uEU4q5P-R_0I1teo_LMcoMeyYJr5P3lkaFbW-E6z-q07MI=w1366-h662-rw)

![database](https://lh5.googleusercontent.com/osvLjcKBQ1NuPpgXwbCYOvtTKmlgKa-bz4eAK1gkyBsn21mOfJlAEBc82EI3eo0inHw1bVWPcuwO5XQ=w1366-h662)

Administrador de eventos realizado con Laravel 5.4

Despues de descargar el proyecto, ir a la carpeta y ejecutar el siguiente comando:

```bash
composer install
```

Derpués crea una nueva base de datos vacía y verifica que los datos estén correctos en el archivo .env :

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

En la carpeta del proyecto, ejecuta el siguiente comando para crear la base de datos del proyecto:

```
php artisan migrate
```

Después es momento de levantar el servidor en el puerto 8000 con el comando:

```
php artisan serve
```

El proyecto ahora es visible en  `localhost:8000` y para acceder a la interfaz de administrador se hace mediante `localhost:8000/login` con las credenciales 
>**email:** `admin@admin.com`   
>**password:** `admin123`

Nota: se puede registrar otro usuario desde la ruta `localhost:8000/register`

Preguntas
¿Qué mejoras propondrías para la solución desarrollada?
Refactorización de código e implementación de framework JS

¿Qué fué lo que más se te complicó de la prueba?
La parte de los archivos JS ya que no había trabajado mucho con el lenguaje ni las peticiones asíncronas, sin embargo unas horas diarias de lectura me hicieron resolverlo con jquery
. 
¿Qué tecnologías usaste? ¿Porque?
Me basé en utilizar Laravel porque es uno de los frameworks más populares, en constante desarrollo por la comunidad y tiene una curva de aprendizaje poco pronunciada

¿Qué metodología usaste, o cual propondrías para el desarrollo de la solución?
Debido a experiencias previas, decidí utilizar SCRUM pues al ser una entrega de una semana, tuve que dividirlo en tareas y tiempo.