<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    protected $fillable =[
		'event_id',
		'first_name',
		'last_name',
		'email',
		'people',
    ];
}
