<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bookings;
use App\Events;
use Illuminate\Support\Facades\Log;
use Validator;

class BookingsController extends Controller
{
    /**
     * Almacena una nueva reservación,validando los datos del formulario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'people' => 'required|numeric',
        ]);

        if ($validator->passes()) {

            $event= Bookings::create([
                'event_id' => $request->input('event_id') ,
                'first_name' => $request->input('first_name') ,
                'last_name' => $request->input('last_name') ,
                'email' => $request->input('email'),
                'people' => $request->input('people') ,
            ]);
            return response()->json(['message'=>'Booking stored'], 200);
        }

        return response()->json(['error'=>$validator->errors()->all()]);

        }catch (\Exception $e){
            Log::critical("Something was wrong storing bookings {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }

    }

    /**
     * Muestra los datos de las personas que hacenunareservación.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $event= Events::with('bookings')->find($id);
            if(!$event){
                return response()->json(['error'=>'Event not found'], 500);
            }
            return response()->json($event->bookings, 200);

        }catch(\Exeption $e){
            Log::critical("Something was wrong showing events: {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }
    }

}