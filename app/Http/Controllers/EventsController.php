<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Events;
use Validator;


class EventsController extends Controller
{
    /**
     * Retorna todos los eventos que han sido registrados
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Events::with('bookings')->get()->toarray();
        return response()->json($events);
    }


    /**
     * Almacena un nuevo evento preia validación de los datos del formulario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'descrip' => 'required|max:250',
                'capacity' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'thumbnail' => 'required|file',
                'slider' => 'required|file',
                'principal' => 'required',
            ]);

            if ($validator->passes()) {
                $event= Events::create([
                    'user_id' =>1,
                    'name' => $request->input('name') ,
                    'descrip' => $request->input('descrip') ,
                    'capacity' => $request->input('capacity'),
                    'start_date' => $request->input('start_date') ,
                    'end_date' => $request->input('end_date') ,
                    'status' => 'Disponible' ,
                    'thumbnail' => $request->file('thumbnail') ,
                    'slider' => $request->file('slider') ,
                    'principal' => $request->input('principal') ,
                ]);
                return response()->json(['message'=>'Event stored', 'id'=>$event->id], 200);
            }
            return response()->json(['error'=>$validator->errors()->all()]);
        }catch (\Exception $e){
            Log::critical("Something was wrong storing events {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }

    }

    /**
     * Retorna la información de un eventoenespecífico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{

            $event= Events::find($id);
            if(!$event){
                return response()->json(['error'=>'Event not found'], 500);
            }
            return response()->json($event, 200);

        }catch(\Exeption $e){
            Log::critical("Something was wrong showing events: {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }
    }

    /**
     * Actualiza la información de un evento específico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   dd($request->all());
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'descrip' => 'required|max:250',
                'capacity' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'principal' => 'required',
            ]);

            if ($validator->passes()) {
                    $event=Events::find($id);
                    if(!$event){
                        return response()->json(['error'=>'Event not found'], 500);
                    }
                    $event->name = $request->input('name');
                    $event->descrip = $request->input('descrip');
                    $event->capacity = $request->input('capacity');
                    $event->start_date = $request->input('start_date');
                    $event->end_date = $request->input('end_date');
                    $event->principal = $request->input('principal');
                    $event->save();
                return response()->json (['message'=>'Event updated'], 200);
            }
            return response()->json(['error'=>$validator->errors()->all()]);
        }catch (\Exception $e){
            Log::critical("Something was wrong updating events {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }
    }

    /**
     * Elimina elregistro de un evento
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try{
            $event= Events::find($id);

            if(!$event){
                return response()->json(['error'=>'Event not found'], 500);
            }
            $event->delete();
            return response()->json (['message'=>'Event deleted'], 200);

        }catch (\Exception $e){
            Log::critical("Something was wrong deleting events {$e->getCode()}, {$e->getLine()}, {$e->getMessage()}");
            return response()->json(['error'=>'Something was wrong :('], 500);
        }
    }
}
