<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Events extends Model
{
    /**
     * Columnas que pueden ser llenadas.
     *
     * @var array
     */
    protected $fillable = [
		'name',
		'user_id',
		'name',
        'descrip',
		'capacity',
		'start_date',
		'end_date',
		'status',
		'thumbnail',
		'slider',
		'principal',
    ];

    public function bookings(){
    	return $this->hasMany('App\Bookings', 'event_id');
    }

    public function setThumbnailAttribute($thumbnail){
    	$this->attributes['thumbnail'] = Carbon::now()->second.$thumbnail->getClientOriginalName();
    	$name=Carbon::now()->second.$thumbnail->getClientOriginalName();
    	\Storage::disk('eventImages')->put($name, \File::get($thumbnail));
    }
    public function setSliderAttribute($slider){
        $this->attributes['slider'] = Carbon::now()->second.$slider->getClientOriginalName();
        $name=Carbon::now()->second.$slider->getClientOriginalName();
        \Storage::disk('eventImages')->put($name, \File::get($slider));
    }
}
