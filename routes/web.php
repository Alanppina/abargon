<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$events = App\Events::with('bookings')->get();
    return view('welcome', compact('events'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/events/{id}', function($id){
	$event = App\Events::with('bookings')->find($id);
	if(is_object($event))
    	return view('public/event', compact('event'));
    return redirect('/');
});