@extends('layouts.public')

@section('content')
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
  @php
    $number=1;
  @endphp
  @foreach($events as $event)
      @if($event->principal==1)
        <li data-target="#myCarousel" data-slide-to="{{$number}}"></li>
        @php
          $number++;
        @endphp
      @endif
    @endforeach
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="http://www.jssor.com/img/home/04.jpg" style="width:100%;">
    </div>

    @foreach($events as $event)
      @if($event->principal==1)
        <div class="item">
          <img src="{{asset('eventImages/'.$event->slider)}}" style="width:100%;">
        </div>
      @endif
    @endforeach

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
  </div>

  <div>
      <h3 class="page-header">Eventos</h3>
      @foreach($events as $event)
        @if($event->start_date>date('Y-m-d'))
          <div class="row">
              <div class="col-md-3">
                  <a class="thumbnail">
                    <img src="{{asset('eventImages/'.$event->thumbnail)}}">
                  </a>
              </div>
              <div class="col-md-8">
                  <h3>{{$event->name}}</h3>
                  <p>{{$event->descrip}}</p>
                  <div class="col-md-1 pull-right"><a href="/events/{{$event->id}}" class="btn btn-primary" role="button">Detalle</a></div>
              </div>
          </div>
          <div class="page-header" style="margin-top: 0"></div>
        @endif
      @endforeach
</div>
@endsection
