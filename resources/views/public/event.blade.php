@extends('layouts.public')

@section('content')
  <h1>{{$event->name}}</h1>
  <div class="row">
    <div class="col-md-12">
      <img src="{{asset('eventImages/'.$event->slider)}}"  style="width:100%;">
      <link rel="stylesheet" href="{{ asset('css/AdminLTE.css') }}">
    <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css') }}">
    </div>
  </div>
  <div class="row" style="margin-top: 5%">
    <div class="col-md-9">
      <p class="text-justify">
        {{$event->descrip}}
      </p>
    </div>
    <div class="col-md-3">
      <p class="text-justify">
        Fecha de inicio: {{$event->start_date}}
      </p>
      <p class="text-justify">
        Fecha fin: {{$event->end_date}}
      </p>
      <p class="text-justify">
        <input hidden type="text" id="available" value="@if($event->bookings->count()==0) {{$event->capacity}} @else {{$event->capacity-$event->bookings()->sum('people')}} @endif">
        Lugares disponibles: @if($event->bookings->count()==0) {{$event->capacity}} @else {{$event->capacity-$event->bookings()->sum('people')}} @endif
      </p>
      <p class="text-justify">
        Destacado: @if($event->principal==1) Si @else No @endif
      </p>
    </div>
  </div>

  <div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
  </div>
  <div class="panel panel-default" style="margin-top: 5%">
    <div class="panel-body">
      <form class="form-horizontal col-sm-7" id="publicForm">
         <input hidden type="text" name="event_id"  id="event_id" value="{{$event->id}}">
        <div class="form-group">
          <label for="first_name" class="col-sm-2 control-label">Nombre</label>
          <div class="col-sm-10">
            <input type="text" name="first_name" class="form-control" id="first_name" >
          </div>
        </div>
          <div class="form-group">
          <label for="last_name" class="col-sm-2 control-label">Apellido</label>
          <div class="col-sm-10">
            <input type="text" name="last_name" class="form-control" id="last_name">
          </div>
        </div>
          <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="email" name="email" class="form-control" id="email" >
          </div>
        </div>
          <div class="form-group">
          <label for="people" class="col-sm-2 control-label">Número de personas</label>
          <div class="col-sm-10">
            <input type="text" name="people" class="form-control" id="people">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-8 col-sm-6">
            <button id='submit' type="button" class="btn btn-primary btn-block">Reservar</button>
          </div>
        </div>
      </form>
      <div class="panel panel-default col-sm-5">
        <div class="panel-body">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fringilla mi ante, id accumsan diam eleifend at. Integer ut commodo lacus. Pellentesque euismod, mauris sed bibendum facilisis, ipsum nisi imperdiet neque, sit amet lacinia arcu augue consectetur odio. Sed nibh magna, gravida et ipsum vitae, placerat placerat ipsum. Morbi dignissim posuere lobortis. 
        </div>
      </div>
    </div>
  </div>
  <div id='notify'></div>
@endsection

@section('scripts');
<script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/publicScriptData.js') }}"></script>
@endsection;