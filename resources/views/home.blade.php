@extends('layouts.app')

@section('content')
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Eventos</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th>Nombre del evento</th>
            <th>Fecha</th>
            <th>Status</th>
            <th>Reservaciones</th>
            <th>Opciones</th>
          </tr>
          </thead>
          <tbody id='data'>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <button type="button" class="btn btn-sm btn-info btn-flat pull-left" data-toggle="modal" data-target="#myModal">
      Agregar Evento
    </button>
    <!-- /.box-footer -->
  </div>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger print-error-msg" style="display:none">
          <ul></ul>
        </div>
        <form id='form' action="/api/events" method="post" enctype="multipart/form-data">
        <input type="text" id="type" hidden>
        <input type="text" id="buttonId" hidden>
          <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" name='name' id="name">
          </div>
          <div class="form-group">
            <label for="descrip">Dscrpción</label>
            <textarea type="text" class="form-control" name='descrip' id="descrip"></textarea>
          </div>
          <div class="form-group">
            <label for="start_date">Fecha de Inicio</label>
            <input type="date" class="form-control" name='start_date' id="start_date" >
          </div>
          <div class="form-group">
            <label for="end_date">Fecha de Fin</label>
            <input type="date" class="form-control" name='end_date' id="end_date">
          </div>
          <div class="form-group">
            <label for="slider">Imagen de portada</label>
            <input type="file" name='slider' id="slider">
          </div>
          <div class="form-group">
            <label for="thumbnail">Imagen miniatura</label>
            <input type="file" name='thumbnail' id="thumbnail">
          </div>
          <div class="form-group">
            <label for="capacity">Capacidad</label>
            <input type="text" class="form-control" name='capacity' id="capacity" >
          </div>
          <div class="form-group">
            <label>Destacado</label>
            <select class="form-control" name='principal' id='principal'>
              <option selected value='0'>No</option>
              <option Value='1'>Si</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id=submit type="button" class="btn btn-primary">Save changes</button>
      </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="deleteLabel">Eliminar Evento</h4>
      </div>
      <div class="modal-body">
        <form id='form' action="/api/events" method="post" enctype="multipart/form-data">
        <input type="text" id="deleteId" hidden>
          ¿DESEAS ELEIMINAR ESTE EVENTO?
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id=deleteSubmit type="button" class="btn btn-primary">Eliminar</button>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger print-error-msg" style="display:none">
          <ul></ul>
        </div>
        <form id='form' action="/api/events" method="post" enctype="multipart/form-data">
        <input type="text" id="type" hidden>
        <input type="text" id="buttonId" hidden>
          <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" name='name' id="name">
          </div>
          <div class="form-group">
            <label for="descrip">Dscrpción</label>
            <textarea type="text" class="form-control" name='descrip' id="descrip"></textarea>
          </div>
          <div class="form-group">
            <label for="start_date">Fecha de Inicio</label>
            <input type="date" class="form-control" name='start_date' id="start_date" >
          </div>
          <div class="form-group">
            <label for="end_date">Fecha de Fin</label>
            <input type="date" class="form-control" name='end_date' id="end_date">
          </div>
          <div class="form-group">
            <label for="slider">Imagen de portada</label>
            <input type="file" name='slider' id="slider">
          </div>
          <div class="form-group">
            <label for="thumbnail">Imagen miniatura</label>
            <input type="file" name='thumbnail' id="thumbnail">
          </div>
          <div class="form-group">
            <label for="capacity">Capacidad</label>
            <input type="text" class="form-control" name='capacity' id="capacity" >
          </div>
          <div class="form-group">
            <label>Destacado</label>
            <select class="form-control" name='principal' id='principal'>
              <option selected value='0'>No</option>
              <option Value='1'>Si</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id=submit type="button" class="btn btn-primary">Save changes</button>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalBookings" tabindex="-1" role="dialog" aria-labelledby="bookingLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="bookingLabel">Reservaciones</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>Nombre</th>
              <th>Email</th>
              <th>Personas</th>
            </tr>
            </thead>
            <tbody id='bookingData'>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('js/scriptData.js') }}"></script>
@endsection
